 



````


$ id
uid=1000(dexter) gid=1000(dexter) groups=1000(dexter)

$ fakechroot fakeroot debootstrap sid /tmp/sid
I: Retrieving Release
I: Retrieving Release.gpg
I: Checking Release signature
...
I: Base system installed successfully.

$ fakechroot fakeroot chroot /tmp/sid apt-get install -q hello
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  hello
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 57.4 kB of archives.
After this operation, 558 kB of additional disk space will be used.
Get:1 http://ftp.us.debian.org/debian/ sid/main hello amd64 2.8-4 [57.4 kB]
Fetched 57.4 kB in 0s (127 kB/s)
Selecting previously unselected package hello.
(Reading database ... 24594 files and directories currently installed.)
Unpacking hello (from .../archives/hello_2.8-4_amd64.deb) ...
Processing triggers for man-db ...
Processing triggers for install-info ...
Setting up hello (2.8-4) ...

$ fakechroot chroot /tmp/sid hello
Hello, world!

````






